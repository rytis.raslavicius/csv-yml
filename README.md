# Install

```
yarn install
```

## YAML TO CSV
```
cat input.yml | node to-csv.js > output.csv
```

## CSV TO YAML
```
cat input.csv | node to-yml.js > output.yml
```

## JSON TO CSV
```
cat input.json | node json-to-csv.js > output.csv
```

## CSV TO JSON
```
cat input.csv | node csv-to-json.js > output.json
```