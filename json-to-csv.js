const { readFileSync } = require("fs");
const { parse } = require("yaml");
const { flatten } = require("flat");
const stringify = require("csv-stringify");
const toString = require("stream-to-string");

async function start() {
  const content = await toString(process.stdin);
  const csv = stringify({ delimiter: "," });
  const value = parse(content);

  csv.pipe(process.stdout);

  Object.keys(value).forEach(key => csv.write([key, value[key]]));

  csv.end();
}
start().catch(console.error);
