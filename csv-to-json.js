const parse = require("csv-parse");

async function start() {
  const result = {};
  const parser = process.stdin.pipe(parse({ delimiter: "," }));

  for await (const [key, value] of parser) {
    result[key] = value;
  }

  process.stdout.write(JSON.stringify(result, null, 2));
}
start().catch(console.error);
