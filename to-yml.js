const parse = require("csv-parse");
const { stringify } = require("yaml");
const { unflatten } = require('flat');

async function start() {
  const result = {};
  const parser = process.stdin.pipe(parse({ delimiter: "," }));

  for await (const [key, value] of parser) {
    result[key] = value;
  }

  process.stdout.write(stringify(unflatten(result)));
}
start().catch(console.error);
